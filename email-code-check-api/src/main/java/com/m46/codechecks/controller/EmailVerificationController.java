package com.m46.codechecks.controller;

import com.m46.codechecks.model.EmailVerification;
import com.m46.codechecks.model.EmailVerificationState;
import com.m46.codechecks.model.VerificationRequest;
import com.m46.codechecks.service.EmailVerificationService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/email")
public class EmailVerificationController {
    private final EmailVerificationService verificationService;

    @GetMapping(path = "/{email}")
    @ResponseStatus(HttpStatus.OK)
    public EmailVerificationState getVerificationState(
            @PathVariable String email
    ){

        return verificationService.getVerificationState(email);
    }

    @PostMapping(path = "/send")
    @ResponseStatus(HttpStatus.CREATED)
    public void requestEmailVerification(

            @RequestBody VerificationRequest verificationRequest
    ) {
        log.info("New verification for {} {}", verificationRequest.getFirstName(), verificationRequest.getEmail());
        System.out.println("EMAIL VERIFY : "+verificationRequest.getEmail()+","+verificationRequest.getFirstName());
        verificationService.requestEmailVerification(
                verificationRequest.getEmail(),
                verificationRequest.getFirstName());
    }

    @PostMapping(path = "/check")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> verify(

            @RequestBody EmailVerification verification
    ) {
        log.info("Verify {} with code '{}'", verification.getEmail(), verification.getVerificationCode());
        verificationService.verify(verification.getEmail(), verification.getVerificationCode());

            RestTemplate restTemplate = new RestTemplate();
        System.out.println("EMAIL "+verification.toString());
            String url = "http://164.90.238.50:8082/api/home/question";
           return ResponseEntity.ok(restTemplate.getForObject(url,String.class));

    }
}
