package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.Question;
import com.m46.codechecks.model.dto.QuestionDto;
import com.m46.codechecks.service.VerificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/home")
public class QuestionsCheckController {

    private final VerificationService verificationService;

    @GetMapping(path = "/question")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<QuestionDto> getQuestion(){
        Question questionDto = verificationService.getAllQuestion();
        QuestionDto q = new QuestionDto();
        q.setId(questionDto.getQuestionId());
        q.setQuestion(questionDto.getQuestion());
              System.out.println("ID QUESTION "+q.getId());
        return ResponseEntity.ok(q);
    }

    @PostMapping(path = "/answer")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> getAnswer(@RequestBody Question question){
        System.out.println("ID CONTROLLER "+question.getQuestionId());
        boolean bool = verificationService.getQuestion(question);

        if(!bool) {
            RestTemplate restTemplate = new RestTemplate();

            String url = "http://164.90.238.50:8082/api/home/question";

            return ResponseEntity.ok(restTemplate.getForObject(url,String.class));
        }
        System.out.println("QUESTION: "+question.toString());
        return ResponseEntity.ok(bool);
    }

}
