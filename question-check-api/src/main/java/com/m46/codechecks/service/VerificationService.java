package com.m46.codechecks.service;


import com.m46.codechecks.model.dto.Question;
import com.m46.codechecks.model.entity.QuestionEntity;
import com.m46.codechecks.repository.VerificationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;


import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
@Slf4j
public class VerificationService {

    private final VerificationRepository verificationRepository;

    private final ModelMapper modelMapper;

    public Boolean getQuestion(Question question) {
        System.out.println("ID "+question.getQuestionId());
        Optional<QuestionEntity> questionEntity = verificationRepository.findById(question.getQuestionId());

        Question question1 = modelMapper.map(questionEntity,Question.class);
        System.out.println("SerVICE "+question1.toString());
        if(!questionEntity.get().getAnswer().equals(question.getAnswer())) {
            return false;
        }
        return true;
    }

    public Question getAllQuestion() {
        List<QuestionEntity> list = verificationRepository.findAll();

            QuestionEntity question = list.get((int) (1+Math.random() * list.size()-1));
            Question questionDto = modelMapper.map(question,Question.class);
            return questionDto;

    }
}
