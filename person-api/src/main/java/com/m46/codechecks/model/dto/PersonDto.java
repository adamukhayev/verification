package com.m46.codechecks.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PersonDto {

    private int id;

    private String email;

    private String name;

    private String surname;

    private String patronymic;

    private ProductsDto productsDto;
}
