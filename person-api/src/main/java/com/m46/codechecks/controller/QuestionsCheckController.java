package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.EmailVerifiCodeDto;
import com.m46.codechecks.model.dto.PersonDto;


import com.m46.codechecks.service.PersonService;
import com.m46.codechecks.service.ProductsService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/add")
public class QuestionsCheckController {

    private final PersonService personService;

    private final ProductsService productsService;

    @PostMapping(path = "/send")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> check(@RequestBody PersonDto personDto) {
        System.out.println("Zashel");
        EmailVerifiCodeDto question = new EmailVerifiCodeDto();
        question.setEmail(personDto.getEmail());
        question.setFirstName(personDto.getName());

        RestTemplate restTemplate = new RestTemplate();

        try {
            System.out.println("Zashel RestTemplate");
            String url = "http://164.90.238.50:8081/api/email/send";
            System.out.println("Zashel RestTemplate1");
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<?> entity = new HttpEntity<>(question, headers);
            System.out.println("QUESTION: "+question.getEmail()+","+question.getFirstName()+","+personDto.getProductsDto().getName()+","+personDto.getProductsDto().getStatus());
            restTemplate.postForObject(url, entity, String.class);

        }catch (RuntimeException e){
            throw new RuntimeException("ERROR EMAIL");
        }


        PersonDto personDto1 = personService.addPerson(personDto);
        if(Objects.nonNull(personDto1)) {
            System.out.println("CREATE PERSON "+personDto.toString());
            personDto.getProductsDto().setPersonId(personDto1.getId());
            productsService.addProducts(personDto.getProductsDto());
            return ResponseEntity.ok(personDto1);
        }else {
            throw new RuntimeException("ERROR CREATE DATA BASE");
        }


    }
    @GetMapping(name = "/update")
    public void UpdateProduct(@RequestParam(value = "status",required = false) String status){

    }
}
