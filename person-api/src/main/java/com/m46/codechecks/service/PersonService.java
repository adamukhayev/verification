package com.m46.codechecks.service;

import com.m46.codechecks.model.dto.PersonDto;
import com.m46.codechecks.model.entity.PersonEntity;
import com.m46.codechecks.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    private final ModelMapper modelMapper;

    public PersonDto addPerson(PersonDto personDto) {
        PersonEntity personEntity = personRepository.save(modelMapper.map(personDto, PersonEntity.class));
        personDto = modelMapper.map(personEntity,PersonDto.class);

        return personDto;
    }
}
